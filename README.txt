
-- SUMMARY --

ExternalAuth allows to configure & setup cookies, which could be used
to check logged in user's authentication of drupal for any
external file/webservice, which is not possible to use within drupal.

After enabling this module site administrators are then able to
setup a cookie's name & key (which is used to encrypt/decrypt user information).

-- CONFIGURATION --

* Configure at Administration » Configuration » ExternalAuth.


-- USAGE --

* Site Administrator can enter a cookie & key name in the configuration form
* then Developers can use it as follow
  $arr = mcrypt_decrypt(MCRYPT_DES, 'yourkey',
         $_COOKIE['yourcookie'], MCRYPT_MODE_ECB);
  $userinfo = (unserialize($arr));
